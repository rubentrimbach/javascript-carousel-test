var $ = require('jquery');
window.jQuery = window.$ = $; // required for bootstrap/selectize etc

var MicroPlugin = require('microplugin');
window.MicroPlugin = MicroPlugin;   // required for selectize

require('bootstrap-sass-twbs');
require('selectize');
