<?php

namespace TweedeGolf\MainBundle\Controller\Frontend;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use TweedeGolf\MainBundle\Entity\Repository\StackRepository;

/**
 * @Route(service="main.controller.frontend.home", path="/")
 */
class HomeController
{
    private $stackRepo;

    public function __construct(StackRepository $stackRepo)
    {
        $this->stackRepo = $stackRepo;
    }

    /**
     * @Route("/", name="homepage")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        return [
            'stacks' => $this->stackRepo->findAll(),
        ];
    }
}
